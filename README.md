In this R code, we created a pamr classifier using glioblastoma data (gene influences). You just need all_batches and inf_gbm_with_cls.Rdata files. The other files are needed to create all_batches files. You can have a look at the end of the code to understand how all_batches was created if you're interested. Thanks to 16 different batches, the idea is to use each one of them as train dataset to train pamr classifier. Next, we used them on test datasets for predictions and we compared which batch as a train dataset is the best one for predictions on test data. You cana also see the HTML output. Have fun !

Author : Marion Estoup

Mail : marion_110@hotmail.fr

Date : September 2023